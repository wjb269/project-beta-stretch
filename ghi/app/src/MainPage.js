import { Link } from 'react-router-dom';

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center" style={{ backgroundColor: "lavender" }}>
      <div className="col-lg-6 mx-auto">
        <h1 className="display-4" style={{ fontFamily: " monospace" }}>InkLink</h1>
        <p className="lead mb-1" style={{ color: "black", fontFamily: "Courier New, monospace" }}>
          All the links needed for covering your body in your favorite tattoo artist's ink!
        </p>
        <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
          <div style={{ marginTop: "30px" }}>
            <img
              src="https://st.depositphotos.com/1341440/1363/v/600/depositphotos_13634797-stock-illustration-black-silhouette-of-rose-with.jpg"
              style={{ width: "150px", height: "150px", objectFit: "cover" }}
              alt="Tattoo artist's ink"
            />
          </div>
          <div className="px-4 py-1 my-1 text-center" style={{ backgroundColor: "lavender" }}>
            <div style={{ display: "flex", justifyContent: "center", marginTop: "30px" }}>
              <div style={{ display: "flex", flexDirection: "column", marginRight: "16px" }}>
                <Link to="/technicians/list" className="btn btn-outline-light shadow" style={{ color: "black", fontFamily: "Courier New, monospace", marginBottom: "16px", width: "200px" }}>Tattoo Studios</Link>
                <Link to="/automobiles/list" className="btn btn-outline-light shadow" style={{ color: "black", fontFamily: "Courier New, monospace", width: "200px" }}>Tattoo Designs</Link>
              </div>
              <div style={{ display: "flex", flexDirection: "column", marginLeft: "16px" }}>
                <Link to="/appointments/add" className="btn btn-outline-light shadow" style={{ color: "black", fontFamily: "Courier New, monospace", marginBottom: "16px", width: "200px" }}>Book a Session</Link>
                <Link to="/sales/add" className="btn btn-outline-light shadow" style={{ color: "black", fontFamily: "Courier New, monospace", width: "200px" }}>AI Designs</Link>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
