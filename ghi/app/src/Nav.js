import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-black" style={{ backgroundColor: "lavender" }}>
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/" style={{ fontFamily: "Courier New, monospace" }}>InkLink</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="row">
            <div className="col">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="manufacturers/add/" style={{ fontFamily: "Courier New, monospace" }}>Add a Tattoo Artist</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="manufacturers/list" style={{ fontFamily: "Courier New, monospace" }}>Tattoo Artists</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="models/add/" style={{ fontFamily: "Courier New, monospace" }}>Add a Tattoo Style</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="models/list" style={{ fontFamily: "Courier New, monospace" }}>Tattoo Styles</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="automobiles/add/" style={{ fontFamily: "Courier New, monospace" }}>Add a Tattoo Design</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="automobiles/list" style={{ fontFamily: "Courier New, monospace" }}>Tattoo Designs</NavLink>
                </li>
                {/* <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="salesperson/add/" style={{ fontFamily: "Courier New, monospace" }}>Add a Salesperson</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="salesperson/list" style={{ fontFamily: "Courier New, monospace" }}>Salespeople</NavLink>
                </li> */}
              </ul>
            </div>
            <div className="col">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="customer/add/" style={{ fontFamily: "Courier New, monospace" }}>Add a Customer</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="customer/list" style={{ fontFamily: "Courier New, monospace" }}>Customers</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="sales/add/" style={{ fontFamily: "Courier New, monospace" }}>Record a Sale</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="sales/list" style={{ fontFamily: "Courier New, monospace" }}>Sales</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" aria-current="page" to="sales/history/" style={{ fontFamily: "Courier New, monospace" }}>Sales History</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="technicians/add/" style={{ fontFamily: "Courier New, monospace" }}>Add a Studio</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="technicians/list/" style={{ fontFamily: "Courier New, monospace" }}>Artist Studios</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="appointments/add/" style={{ fontFamily: "Courier New, monospace" }}>Book an Appointment </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="appointments/list/" style={{ fontFamily: "Courier New, monospace" }}>Appointments</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="service/history/" style={{ fontFamily: "Courier New, monospace" }}>Appointment History</NavLink>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
