import React, { useState } from "react";

function ManufacturerForm() {
    const [name, setName] = useState("");

    const nameChange = (event) => {
        setName(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;

        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const manufacturer = await fetch(manufacturerUrl, fetchConfig);
        if (manufacturer.ok) {
            setName("");
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div
                    className="shadow p-4 mt-4"
                    style={{
                        fontFamily: "Courier New, monospace",
                        backgroundColor: "lavender",
                        borderRadius: "10px",
                    }}
                >
                    <h1>Add a Tattoo Artist</h1>
                    <form id="create-manufacturer-form" onSubmit={handleSubmit}>

                        <div className="form-floating mb-3">
                            <input
                                placeholder="Name"
                                required
                                type="text"
                                name="name"
                                id="name"
                                className="form-control"
                                value={name}
                                onChange={nameChange}
                                style={{ fontFamily: "Courier New, monospace" }}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <button
                            className="btn btn-primary"
                            style={{
                                backgroundColor: "lavender",
                                color: "black",
                            }}
                        >
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;
