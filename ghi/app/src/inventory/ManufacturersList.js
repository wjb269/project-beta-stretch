import React, { useEffect, useState } from "react"


function ManufacturersList() {

    const [manufacturers, setManufacturers] = useState([]);

    async function loadManufacturers() {
        const response = await fetch("http://localhost:8100/api/manufacturers/");
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        loadManufacturers();
    }, []);

    return (
        <div
            className="shadow p-4 mt-4"
            style={{
                fontFamily: "Courier New, monospace",
                backgroundColor: "lavender",
                borderRadius: "10px",
            }}
        >            <h1>Tattoo Artists</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Artists</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers?.map(manufacturer => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table >
        </div>
    );
}

export default ManufacturersList;
