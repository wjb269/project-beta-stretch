import React, { useEffect, useState } from "react"


function SalesForm() {

    const [automobiles, setAutomobiles] = useState([]);
    const [salesperson, setSalesperson] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobile, setAutomobile] = useState('');
    const [salesPeople, setSalespeople] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState(0);

    const automobileChange = (event) => {
        setAutomobile(event.target.value);
    }

    const salespeopleChange = (event) => {
        setSalespeople(event.target.value);
    }

    const customerChange = (event) => {
        setCustomer(event.target.value);
    }

    const priceChange = (event) => {
        setPrice(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.price = price;
        data.automobile = automobile;
        data.sales_person = salesPeople;
        data.customer = customer;

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const sale = await fetch(salesUrl, fetchConfig);
        if (sale.ok) {

            const automobileUrl = `http://localhost:8100/api/automobiles/${automobile}/`;
            const data = { sold: true };
            const fetchConfig = {
                method: "PUT",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json"
                },
            };

            const response = await fetch(automobileUrl, fetchConfig);
            if (response.ok) {

                setPrice('');
                setAutomobile('');
                setSalespeople('');
                setCustomer('');
            }
        }
    }

    async function fetchAutomobile() {
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(automobileUrl);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    async function fetchSalesperson() {
        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespersonUrl);
        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salespeople);
        }
    }

    async function fetchCustomer() {
        const customerUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customerUrl);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    useEffect(() => {
        fetchAutomobile();
        fetchSalesperson();
        fetchCustomer();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create Your Own AI-Generated Design</h1>
                    <form id="create-record-of-sale-form" onSubmit={handleSubmit}>
                        <label htmlFor="automobile_vin">Automobile VIN</label>
                        <div className="form-floating mb-3">
                            <select required placeholder="Automobile VIN" name="automobile_vin" id="automobile_vin" className="form-select" onChange={automobileChange} value={automobile}>
                                <option value="">Choose an Automobile VIN</option>
                                {automobiles?.map(auto => {
                                    if (auto.sold === false) {
                                        return (
                                            <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                                        );
                                    }
                                })}
                            </select>
                        </div>
                        <label htmlFor="salesperson">Salesperson</label>
                        <div className="form-floating mb-3">
                            <select required placeholder="Salesperson" name="salesperson" id="salesperson" className="form-select" onChange={salespeopleChange} value={salesPeople}>
                                <option value="">Choose a Salesperson</option>
                                {salesperson?.map(salespeople => {
                                    return (
                                        <option key={salespeople.id} value={salespeople.id}>{salespeople.first_name} {salespeople.last_name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <label htmlFor="customer">Customer</label>
                        <div className="form-floating mb-3">
                            <select required placeholder="Customer" name="customer" id="customer" className="form-select" onChange={customerChange} value={customer}>
                                <option value="">Choose a Customer</option>
                                {customers?.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <label htmlFor="price">Price</label>
                        <div className="form-floating mb-3">
                            <input required placeholder="Price" id="price" name="price" type="number" min="100" className="form-control" onChange={priceChange} value={price} />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesForm;
