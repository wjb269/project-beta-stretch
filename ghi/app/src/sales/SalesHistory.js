import React, { useState, useEffect } from 'react'

function SalesHistory() {

    const [salespeople, setSalespeople] = useState([]);
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesperson] = useState('');

    const salespersonChange = (event) => {
        setSalesperson(event.target.value);
    }

    async function fetchSalespeople () {
        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespersonUrl);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }

    async function fetchSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }

    useEffect(() => {
        fetchSalespeople();
        fetchSales();
    }, []);

    return (
       <>
       <h1>Salesperson History</h1>
       <div className="form-floating mb-3">
            <select required placeholder="Salesperson" name="sales_person" id="sales_person" className="form-select" onChange={salespersonChange} value={salesperson}>
                    <option value="">Select a Salesperson</option>
                        {salespeople?.map(sales_person => {
                            return (
                                <option key={sales_person.id} value={sales_person.employee_id}>
                                    {sales_person.first_name} {sales_person.last_name}
                                </option>
                                
                            );
                        })}
            </select>                 
        </div>
        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {sales.filter(salespeople => salespeople.sales_person.employee_id == salesperson).map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.sales_person.first_name} {sale.sales_person.last_name}</td>
                            <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>${sale.price}.00</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
       </>
    );
}

export default SalesHistory;