import React, { useState, useEffect } from "react"

function AppointmentForm() {

    const [date, setDate] = useState([]);
    const [time, setTime] = useState([]);
    const [technicians, setTechnicians] = useState([]);
    const [automobiles, setAutomobiles] = useState("");
    const [customer, setCustomer] = useState("");
    const [person, setPerson] = useState("");
    const [reason, setReason] = useState("");

    const automobilesChange = (event) => {
        setAutomobiles(event.target.value);
    };

    const customerChange = (event) => {
        setCustomer(event.target.value);
    };

    const dateChange = (event) => {
        setDate(event.target.value);
    };

    const timeChange = (event) => {
        setTime(event.target.value);
    };

    const personChange = (event) => {
        setPerson(event.target.value);
    };

    const reasonChange = (event) => {
        setReason(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.customer = customer;
        data.date_time = `${date} ${time}`;
        data.technician = person;
        data.vin = automobiles;
        data.reason = reason;

        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const appointment = await fetch(appointmentUrl, fetchConfig);
        if (appointment.ok) {

            setAutomobiles("");
            setDate("");
            setTime("");
            setPerson("");
            setCustomer("");
            setReason("");
        }
    }

    async function fetchTechnician() {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const response = await fetch(technicianUrl);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
        fetchTechnician();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Book a Session</h1>
                    <form id="create-record-of-an-appointment-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" onChange={automobilesChange} value={automobiles} />
                            <label htmlFor="vin">Artist ID</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" onChange={customerChange} value={customer} />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Date" required type="date" name="date" id="date" className="form-control" onChange={dateChange} value={date} />
                            <label htmlFor="date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Time" required type="time" name="time" id="time" className="form-control" onChange={timeChange} value={time} />
                            <label htmlFor="time">Time</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select required placeholder="Technician" id="technician" className="form-select" onChange={personChange} value={person}>
                                <option value="">Select</option>
                                {technicians?.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                                    );
                                })}
                            </select>
                            <label htmlFor="technician">Artists</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" onChange={reasonChange} value={reason} />
                            <label htmlFor="reason"> Anything We Should Know?</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AppointmentForm;
