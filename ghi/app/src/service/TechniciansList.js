import React, { useEffect, useState } from "react"

function TechniciansList() {

    const [technicians, setTechnicians] = useState([]);

    async function loadTechnicians() {
        const response = await fetch("http://localhost:8080/api/technicians/");
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
        loadTechnicians();
    }, []);

    return (
        <div>
            <h2>Tattoo Studios</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>ID: Style</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians?.map(technician => {
                        return (
                            <tr key={technician.id}>
                                <td>{technician.employee_id}</td>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table >
        </div >
    );
}

export default TechniciansList;
