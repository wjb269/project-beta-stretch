from django.views.decorators.http import require_http_methods
from sales_rest.models import AutomobileVO, Sale, Customer, Salesperson
from django.http import JsonResponse
from sales_rest.encoders import (
    AutomobileVOEncoder,
    SalesPersonEncoder,
    SaleEncoder,
    CustomerEncoder,
)
import json

# Create your views here.


@require_http_methods(["GET"])
def api_list_automobile_vo(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            salespeople = Salesperson.objects.create(**content)
            return JsonResponse(
                salespeople,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Unable to create salesperson"},
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_salesperson(request, id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Salesperson doesn't exist"},
                status=404,
            )
            return response
    else:
        try:
            salesperson = Salesperson.objects.get(employee_id=id).delete()
            return JsonResponse(
                {"confirmation": "Salesperson deleted"},
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson doesn't exist"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Unable to create customer"},
                status=400,
            )


@require_http_methods(["GET", "DELETE"])
def api_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404,
            )
    else:
        try:
            customer = Customer.objects.filter(id=id).delete()
            return JsonResponse(
                {"confirmation": "Customer deleted"},
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def api_sales(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            sales = Sale.objects.filter(automobile=automobile_vo_id)
        else:
            sales = Sale.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder=SaleEncoder,
                safe=False,
            )
    else:
        content = json.loads(request.body)
        try:
            sales_person = Salesperson.objects.get(id=content["sales_person"])
            content["sales_person"] = sales_person
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person doesn't exist"},
                status=404,
            )
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer doesn't exist"},
                status=404,
            )
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile doesn't exist or has been sold"},
                status=404,
            )
        sales = Sale.objects.create(**content)
        return JsonResponse(sales, encoder=SaleEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_sale(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sales doesn't exist"},
                status=404,
            )
    else:
        try:
            sale = Sale.objects.get(id=id).delete()
            return JsonResponse({"confirmation": "Sale has been deleted"})
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale doesn't exist"},
                status=404,
            )
