from common.json import ModelEncoder
from service_rest.models import Technician, AutomobileVO, Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin",
        "status",
        "customer",
        "date_time",
        "technician",
        "reason",
        "id",
        "vip",
    ]
    encoders = {"technician": TechnicianEncoder()}
